import matplotlib.pyplot as plt


def plotCM(CM, CM_title, CM_subtitles):
    """
    plotCM

    USAGE: 
    CM_fig, ax = functionName(CM, CM_title, CM_subtitles)
    
    DESCRIPTION:
    Plots reslts of multiple confusion matricies in a subplot 
    
    INPUTS:  
    CM           -   A dictionary where each key refferes to the the output of sklearns
                     ConfusionMatrixDisplay function
    CM_title     -   string containing title
    CM_subtitles -   string ontaining the subtitle
    
    OUTPUTS: 
    ... - ...
    ... - ...
    """
    
    CM_fig, ax = plt.subplots(1,len(CM))
    CM_fig.suptitle(CM_title[0], fontsize=20)

    for i, key in enumerate(CM.keys()):

        CM[key].plot(ax = ax[i])
        ax[i].set_title(CM_subtitles[i])

    CM_fig.tight_layout(pad=3.0)

    return CM_fig, ax