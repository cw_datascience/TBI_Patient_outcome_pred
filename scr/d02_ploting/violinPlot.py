import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler  

# My custom functions 
from d00_utils.saveFile import saveFileToDirPath 


def plot_violin_AvAll(df):
    """
    plot_violin_AvAll(df)

    USAGE: 
    out = plot_violin_AvAll(df)
    
    DESCRIPTION:
    Makes a violin plot of the Av_ALL data set 
    (i.e the one where all the voxels have been averaged)
    
    INPUTS:  
    df - data where all the voxels have been averaged
    
    OUTPUTS: 
    produces a violin plot of the data scaled to the mean 
    and standard deviation using StandardScaler()
    
    """

    sc = StandardScaler()
    df.iloc[:,1:5] = sc.fit_transform(df.iloc[:,1:5])
    # rename the feature labes as they would appearch in the chapter
    df.rename(columns={'sPCr_sATPg':'PCr/\u03B3ATP'}, inplace=True)
    df.rename(columns={'sPCr_All.1':'PCr/tmp'}, inplace=True)
    df.rename(columns={'sATPg_All.1':'\u03B3ATP/tmp'}, inplace=True)
    df.rename(columns={'pH':'Brain pH'}, inplace=True)

    # change the class names
    df.Outcome_B.replace(2,'HC', inplace=True)
    df.Outcome_B.replace(1, 'fav', inplace=True)
    df.Outcome_B.replace(0, 'unfav', inplace=True)

    # reorgnaise data set for violin plot
    df = df.drop(columns=['Code'])
    df = df.set_index(['Outcome_B'], append=True)
    df = df.stack(level=0)

    df = df.reset_index()
    df.drop(columns={'level_0'}, inplace=True)

    # nename the labels in the mrs_e_av_vio data frame
    df.rename(columns={'level_2':'ML features'}, inplace=True)
    df.rename(columns={0:'Scaled $^{31}$P signals'}, inplace=True)
    ax = sns.violinplot(x="ML features", y="Scaled $^{31}$P signals", hue="Outcome_B",
                            data=df, palette="Set2", inner="quartile",
                            linewidth=0.8, hue_order = ['HC', 'fav', 'unfav'])
        

    ax.legend(loc='upper center')
    plt.rcParams["axes.labelsize"] = 10


    plt.xlabel('Machine Learning Features', fontsize=18)
    plt.ylabel('Scaled $^{31}$P signal', fontsize=18)
    plt.tick_params(axis="x", labelsize=15, rotation=45)
    plt.tick_params(axis="y", labelsize=15)
    plt.show()

    # Export to intermidiate folder 
    outname = 'violin_AvAll.pdf'
    outdir = '../data/d03_plots/violinPlots'
    saveFileToDirPath(ax.get_figure(), outname, outdir, 2)





def plot_violin_ROI(df):
    """
    plot_violin_ROI(df)

    USAGE: 
    out = plot_violin_ROI(df)
    
    DESCRIPTION:
    Makes a violin plot of the Av_ROI data set 
    (i.e the one where all the voxels averaged into regions of interest)
    
    INPUTS:  
    df - data where all the voxels averaged into regions of interest
    
    OUTPUTS: 
    produces a violin plot of the data scaled to the mean 
    and standard deviation using StandardScaler()
    
    """

    sc = StandardScaler()
    df.iloc[:, 2:14] = sc.fit_transform(df.iloc[:, 2:14])
    #df.rename(columns={'sPCr_sATPg':'PCr/\u03B3ATP'}, inplace=True)
    #df.rename(columns={'sPCr_All.1':'PCr/tmp'}, inplace=True)
    #df.rename(columns={'sATPg_All.1':'\u03B3ATP/tmp'}, inplace=True)
    #df.rename(columns={'pH':'Brain pH'}, inplace=True)

    # change the class names

    df.Outcome_B = df.Outcome_B.replace(2,'HC')
    df.Outcome_B = df.Outcome_B.replace(1, 'fav')
    df.Outcome_B = df.Outcome_B.replace(0, 'unfav')


    # rename the metabolite names labels

    df.rename(columns={'sPCr_sATPg':'PCr/\u03B3ATP'}, inplace=True)
    df.rename(columns={'sPCr_All.1':'PCr/tmp'}, inplace=True)
    df.rename(columns={'sATPg_All.1':'\u03B3ATP/tmp'}, inplace=True)
    df.rename(columns={'spH':'Brain pH'}, inplace=True)



    # reorgnaise data set for violin plot
    df.drop(columns=['Code'], inplace=True)

    df = df.set_index(['Outcome_B'], append=True)

    df = df.stack(level=0)

    df = df.stack(level=0)
    df = df.reset_index()

    df.rename(columns={'level_2':'Metabolite'}, inplace=True)
    df.rename(columns={0:'signals'}, inplace=True)



    # make multi pannel violin plot

    # I need to change the order of the metabolites in the data frame here
    f = sns.catplot(x = "Metabolite", y = "signals",  sharey=False, 
                        hue = "Outcome_B", col_wrap = 3, kind = 'violin', palette="Set2", 
                        inner="quartile", col = "Anatomical pos", data = df,
                        order = ['PCr/\u03B3ATP', 'PCr/tmp','\u03B3ATP/tmp', 'Brain pH'], 
                        hue_order = ['HC', 'fav', 'unfav'], col_order = ['Frontal', 'Central', 'Pariental'],
                        legend_out=True)


    # change axes and titles
    axes = f.axes.flatten() # allows use of any matplotlib method you like to tweak the plot.

    # Define label  sizes
    titFnt = 50
    Xaxis = 40
    Yaxis = 25
    Xtick = 20
    Ytick = Xtick
    Xpad = 20


    axes[0].set_title('Frontal', fontsize=titFnt)
    axes[1].set_title('Central', fontsize=titFnt)
    axes[2].set_title('parietal', fontsize=titFnt)   # It is spelt 'parietal' not 'Pariental'

    axes[0].set_xlabel('', fontsize=Xaxis, labelpad=Xpad)
    axes[1].set_xlabel('Machine Learning Features', fontsize=Xaxis, labelpad=Xpad)
    axes[2].set_xlabel(' ', fontsize=Xaxis, labelpad=Xpad)

    # x tick paramas
    axes[0].tick_params(axis="x", labelsize=Xtick, rotation=45)
    axes[1].tick_params(axis="x", labelsize=Xtick, rotation=45)
    axes[2].tick_params(axis="x", labelsize=Xtick, rotation=45)

    # y tick paramas
    axes[0].tick_params(axis="y", labelsize=Xtick)
    axes[1].tick_params(axis="y", labelsize=Xtick)
    axes[2].tick_params(axis="y", labelsize=Xtick)

    axes[0].set_ylabel('Scaled $^{31}$P signal', fontsize=Yaxis)


    # ledgend
    f._legend.set_title('')

    plt.setp(f._legend.get_texts(), fontsize=15)


    # Export to intermidiate folder 
    outname = 'violin_ROI.pdf'
    outdir = '../data/d03_plots/violinPlots'
    saveFileToDirPath(f, outname, outdir, 2)
    
    

