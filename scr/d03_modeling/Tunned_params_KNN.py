import numpy as np
from sklearn.model_selection import LeaveOneOut
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV

def f_Tunned_params_KNN(features, feature_set, labels, HyperParam_grid): #takes full feature array, selected best features, and labels
    """
    USAGE: 
    f_Tunned_params_KNN(features, feature_set, labels, HyperParam_grid)
    
    DESCRIPTION:
    KNN hyper parameter (c and gamma) optimisation through a grid search. 
    CV procedure used is LeaveOneOut.  
    
    INPUTS:  
    same as f_Tunned_params_SVMRBF
    
    OUTPUTS: 
    
    """

    # KNeighborsClassifier PARAMETER OPTIMISATION
    inner_best_features = features[:,feature_set]
    cross_val = LeaveOneOut() # cross_val changed to loo
    clf = GridSearchCV(estimator=KNeighborsClassifier(weights='distance'), param_grid=HyperParam_grid, cv=cross_val) # define a model with parameter tunning
    clf.fit(inner_best_features,np.ravel(labels)) # fit a model
    best_params = clf.best_params_ # gives optimised C and Gamma parameters
    k = best_params['n_neighbors'] 
    accuracy_p_tunned = clf.best_score_
    return k, accuracy_p_tunned