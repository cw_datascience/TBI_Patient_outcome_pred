import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import LeaveOneOut
import functools

from d03_modeling.SVM_RFE import SVM_RFE
from d03_modeling.SVMRFB_clf import f_SVMRFB_clf
from d03_modeling.KNN_clf import f_KNN_clf



def f_NestedL00CV(X_train, y_train, CLASSIFIER, FEATURE_SELECTOR, HyperParam_grid):
    """
    USAGE: 
    f_NestedL00CV(X_train, y_train, CLASSIFIER, FEATURE_SELECTOR, HyperParam_grid)
    
    DESCRIPTION:
    Performs nested LOOCV
    
    INPUTS:  
    X_train - X training data to be validated 
    y_train - Y traing data to be validated
    CLASSIFIER - clasffier type (SVM-RBF = 1, KNN = 2)
    FEATURE_SELECTOR - feature sleection on = 1, feature selection off = 2
    HyperParam_grid - The hyper parameters values used for the grid search  
    
    OUTPUTS: 
    If clf =  SVM:
           acc_LOOCV = leave one out CV accuracy
           C_val_loop = C values seleced in the cross validation 
           gamma_loop = gamma values selected in the cross validation 
    If clf = KNN:
           acc_LOOCV = leave one out CV accuracy
           k_val_loop = k values seleced in the cross validation 
        
    """

    # VALIDATION PART 
    '''
    features = X_train_sc
    labels_df = y_train  # I need to reindex y_train
    labels_df = pd.DataFrame(data=labels_df)
    labels_df = labels_df.reset_index(drop=True)       
    '''
    
    #% create a data frame to store model hyperparams selected in loocv for SVM (C&gamma vals) and KNN (k vals)
    # This gives information on model stability 
    
    
    
    # pass in unscale training data to CV pipline
    features = X_train
    features = features.reset_index(drop=True)
    labels_df = y_train  # I need to reindex y_train
    labels_df = pd.DataFrame(data=labels_df)
    labels_df = labels_df.reset_index(drop=True)       
    
    
   # from sklearn.preprocessing import StandardScaler 
    sc = StandardScaler() # For scaling in CV loop
    
     
    # Change StratifiedKFold to leaveOneOutCV
    #kf = StratifiedKFold(n_splits=10, shuffle=True) # split into 10 different training and testing samples with suffling
    #kf.get_n_splits(X=features,y=labels) 
    
    loo = LeaveOneOut()
    loo.get_n_splits(X=features,y=labels_df)
    
    inner_loop_acc_clf = [] # prealocate
    inner_loop_pred = [] # prealocate what the classifier actually predicted
    gamma_loop = [] #  preallocate
    C_val_loop = [] # preallocate
    k_val_loop = [] #  preallocate
    
    
    for train_index, test_index in loo.split(features,labels_df): # start loocv nested validation pouter loop 
        train_data, validation_data = features.iloc[train_index], features.iloc[test_index] # get inner loop training data and outer loop valdiaiton data
        train_labels, validation_labels = labels_df.iloc[train_index], labels_df.iloc[test_index] # get inner loop training labels and outer loop valdiaiton labels
        
        # scale the train data and use the same scaling factore to scale the validation set
        train_data = sc.fit_transform(train_data)
        validation_data = sc.transform(validation_data)
        
        
        # Add feature selection or not 
        if FEATURE_SELECTOR == 1: #SVM-RFE
            # SVM-RBF
            feature_set = SVM_RFE(features=train_data,labels=train_labels)
        elif FEATURE_SELECTOR == 2: # no feature selection 
            feature_set = np.array(list(range(0,(features.shape[1]),1)))
            
    
        
        # Perform classification on inner loop     
        
        if CLASSIFIER == 1:    # SVM-RBF
            acc, C, gamma, predictions = f_SVMRFB_clf(train_data, feature_set, train_labels, validation_data, validation_labels, HyperParam_grid)
            inner_loop_acc_clf.append(acc)
            inner_loop_pred.append(predictions) # redord all the predictions made
            C_val_loop.append(C)
            gamma_loop.append(gamma)
            
        elif CLASSIFIER == 2:   # KNN
            acc, k, predictions = f_KNN_clf(train_data,feature_set,train_labels, validation_data, validation_labels, HyperParam_grid)
            inner_loop_acc_clf.append(acc)
            inner_loop_pred.append(predictions) # redord all the predictions made
            k_val_loop.append(k)
            
        
    acc_LOOCV = np.mean(inner_loop_acc_clf)  # inner loop accuracy
    
    
    if CLASSIFIER == 1:   # SVM-RBF
        return acc_LOOCV, C_val_loop, gamma_loop, inner_loop_acc_clf, inner_loop_pred
    elif CLASSIFIER == 2:   # KNN
        return acc_LOOCV, k_val_loop, inner_loop_acc_clf, inner_loop_pred