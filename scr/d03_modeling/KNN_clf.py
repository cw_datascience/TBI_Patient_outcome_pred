import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score

from d03_modeling.Tunned_params_KNN import f_Tunned_params_KNN

def f_KNN_clf(train_data, feature_set, train_labels, validation_data, validation_labels, HyperParam_grid):
    """
    USAGE: 
    f_KNN_clf(train_data, feature_set, train_labels, validation_data, validation_labels, HyperParam_grid)
    
    DESCRIPTION:
    Trains KNN Classifiers using f_Tunned_params_KNN(...) to select best
    hyper parameters (k)
    
    INPUTS:  
    train_data -  input training data
    feature_set - feature set used to classify 
    train_labels - class of training data 
    validation_data - Data to validate
    validation_labels - class of validation data
    HyperParam_grid - The hyper parameters values used for the grid search  
    
    OUTPUTS: 
    acc - acuracy of the model in predicting validation set
    k - k value selected to predict the validation set
    """
    
    k,_ = f_Tunned_params_KNN(train_data,feature_set,train_labels, HyperParam_grid) # fild optimal n_neighbors (or k) value           
    model = KNeighborsClassifier(n_neighbors=k, weights='distance')
    model.fit(train_data[:,feature_set],np.ravel(train_labels)) # fit model
    predictions = model.predict(validation_data[:,feature_set]) # predicted labels with a model on a validation data
    acc = accuracy_score(validation_labels,predictions)   
    
        
    return acc, k, predictions