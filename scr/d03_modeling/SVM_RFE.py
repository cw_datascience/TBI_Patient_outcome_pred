import pandas as pd
import numpy as np
from sklearn.svm import SVC # SVM
from sklearn.model_selection import cross_val_score  # for parameter optimisation using grid_search, for crossvalidation, for Kfold split 
from sklearn.model_selection import LeaveOneOut

import operator # for finding highest value in dictionary


def SVM_RFE(features, labels):

    # USAGE: 
    # SVM_RFE(features,labels)
    #
    # DESCRIPTION:
    # Takes in training data (labeld here as features) and performs SVM-RFE to 
    # find the the best features  
    #
    # INPUTS:  
    # features - Is the data with all the corresponding features
    # labels - labels of the train data 
    #
    # OUTPUTS: 
    # feature_set - the new feature set used for classification 
    #

    
    # I has such a small amount of features that don't need the prealocation stage
    #prealocation
    Best_features = np.arange(0,features.shape[1]) # to retain feature order in the original feature space
    features_inner = features
    #loop starts here to select best features up to 20
    while features_inner.shape[1] > 20:
        model = SVC(kernel='linear', C=1)
        model.fit(features_inner,np.ravel(labels)) # fit linear model to inner features
        w = np.abs(model.coef_) # find feature weight vector
        worst_feature = w.argmin(axis=1)[0] # find lowest ranked feature
        features_inner = np.delete(features_inner,worst_feature,1) # remove worst feature from featurearray
        Best_features = np.delete(Best_features,worst_feature) # to retain labels of best features after the loop

    best_feature_recrd = {}
    accuracies= {}
    for n in list(range(3)):   # chris w : change to from list(range(12)) to list(range(3)) as 4 is the max number of features I have
        model = SVC(kernel='linear', C=1)
        model.fit(features_inner,np.ravel(labels)) # fit linear model to inner features
        w = np.abs(model.coef_) # find feature weight vector
        worst_feature = w.argmin(axis=1)[0] # find lowest ranked feature
        features_inner = np.delete(features_inner,worst_feature,1) # remove worst feature from featurearray
        Best_features = np.delete(Best_features,worst_feature) # to retain labels of best features after the loop
        
        # find cross-validated accuracy
        model2 = SVC(kernel='linear', C=1)
        scores = cross_val_score(model2, features_inner, np.ravel(labels), cv=LeaveOneOut()) #int(labels.shape[0]/2)) # change cv to loo
        acc = scores.mean() # crossvalidation accuracy
    
        # record inner loop results into dictionary
        best_feature_recrd[n]=Best_features
        accuracies[n]=acc

    dict_key = max(accuracies.items(), key=operator.itemgetter(1))[0] # find dictionary key (loop) in which highest accuracy was achieved
    feature_set = best_feature_recrd[dict_key] # feature set is selected from SVM_RFE loops 20 t0 8 last loops where highest accuracy was achieved
    #accuracy = accuracies[dict_key] # accuracy atchieved by selected features in the training set
    # END of SVM-RFE
    return feature_set
