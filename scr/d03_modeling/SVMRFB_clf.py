import numpy as np
from sklearn.preprocessing import StandardScaler 
from sklearn.svm import SVC # SVM
#from sklearn import linear_model # For logistic regression
from sklearn.metrics import accuracy_score # for accuracy estimates

from d03_modeling.Tunned_params_SVMRBF import f_Tunned_params_SVMRBF





def f_SVMRFB_clf(train_data, feature_set, train_labels, validation_data, validation_labels, HyperParam_grid):
    #
    # USAGE: 
    # f_SVMRFB_clf(train_data, feature_set, train_labels, validation_data, validation_labels, HyperParam_grid)
    # 
    #
    # DESCRIPTION:
    # Trains SVM Classifiers using f_Tunned_params_SVMRBF(...) to select best
    # hyper parameters (C and gamma)
    #
    # INPUTS:  
    # train_data -  input training data
    # feature_set - feature set used to classify 
    # train_labels - class of training data 
    # validation_data - Data to validate
    # validation_labels - class of validation data
    # HyperParam_grid - The hyper parameter values used for the grid search
    #
    # OUTPUTS: 
    # acc - acuracy of the model in predicting validation set
    # C - C value selected to predict the validation set
    # gamma - gamma value selected to predict the validation set
    
    
    C, gamma,_ = f_Tunned_params_SVMRBF(train_data, feature_set, train_labels, HyperParam_grid) # fild optimal C and Gamma parameters
    model = SVC(C=C,kernel='rbf',gamma = gamma, class_weight='balanced',probability=True) # create SVM model with optimal parameters
    model.fit(train_data[:,feature_set],np.ravel(train_labels)) # fit model
    predictions = model.predict(validation_data[:,feature_set]) # predicted labels with a model on a validation data
    acc = accuracy_score(validation_labels,predictions)   # unbalance accuracy score
    #acc = balanced_accuracy_score(validation_labels,predictions)  # balanced accuracy score
    
    # change type of prediction value to a float, so not in an array
    predictions = predictions[0].astype(float)
    
    return acc, C, gamma, predictions
