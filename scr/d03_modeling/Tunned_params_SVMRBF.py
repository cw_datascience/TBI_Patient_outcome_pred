import numpy as np
from sklearn.metrics import confusion_matrix
# import classifiers 
from sklearn.svm import SVC # SVM
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import LeaveOneOut



def f_Tunned_params_SVMRBF(features, feature_set, labels, HyperParam_grid): #takes full feature array, selected best features, and labels
    """
    USAGE: 
    f_Tunned_params_SVMRBF(features, feature_set, labels, HyperParam_grid)
    
    DESCRIPTION:
    SVM RBF hyper parameter (c and gamma) optimisation through a grid search. 
    CV procedure used is LeaveOneOut. 
    
    INPUTS:  
    features - train data with corresponding features
    feature_set - mask to select features used when doing the hyper param grid search
    labels - class labels of data
    HyperParam_grid - The hyper parameter values used for the grid search
    
    OUTPUTS: 
    C- C value selected
    gamma - gamma value selected
    accuracy_p_tunned - accuracy
    """

    inner_best_features = features[:,feature_set]
    #cross_val = StratifiedKFold(n_splits=10, shuffle=True)
    cross_val = LeaveOneOut() # cross_val changed to loo
    clf = GridSearchCV(estimator=SVC(kernel="rbf", class_weight='balanced'), param_grid=HyperParam_grid, cv=cross_val) # define a model with parameter tunning -- old grid search using accuracy
    #clf = GridSearchCV(estimator=SVC(kernel="rbf", class_weight='balanced'), scoring= 'balanced_accuracy', param_grid=HyperParam_grid, cv=cross_val) # new grid search using balanced_accuracy
    clf.fit(inner_best_features,np.ravel(labels)) # fit a model
    best_params = clf.best_params_ # gives optimised C and Gamma parameters
    C = best_params['C']
    gamma = best_params['gamma']
    accuracy_p_tunned = clf.best_score_
    return  C, gamma, accuracy_p_tunned
    