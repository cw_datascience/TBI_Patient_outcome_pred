import os
import pandas as np
import matplotlib.pyplot as plt


def saveFileToDirPath(file, outname, outdir, type):
    """
    USAGE: 
    saveFileToDirPath(df, outname, outdir) 
    
    DESCRIPTION:
    Saves a file to the relivant folder. If a folder in the path name 
    does not exist then it gets created      
    
    INPUTS:  
    df        - data frame containing the data/image
    
    outname   -
    
    outdir    - 

    type      - type of file you want to save (1 = dataFrame, 2 = image)
    
    OUTPUTS: 
    AllVox_av - data set where voxels are average on a per person basis (no anatomical information) 
    """

    # Export to intermidiate folder 
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    
    fullname = os.path.join(outdir, outname)

    if type == 1:
        file.to_csv(fullname, index=False)
    elif type == 2:
        file.savefig(fullname)
        
        
        

