import pandas as pd


def f_preProcStov19Data():
    
    """
    USAGE: 
    f_preprocStov19Data() 
    
    DESCRIPTION:
    uploads Stovels data set and performes some pre processing 
    steps where redundant informatino is removed 
    
    INPUTS: 
    none
    
    OUTPUTS: 
    relivant data MRSi_anatomical_gd_bad.csv stored in a pandas data frame
    
    """
    # Extract data
    mrs_i = pd.read_csv('..\data\d01_raw\MRSi_anatomical_gd_bad_ForOrganisedProject.csv')    

    # only retain relivant columns 
    mrs_i = mrs_i[['Code', 'Voxel', 'sPCr_sATPg','sPCr_All.1', 
                        'sATPg_All.1','spH', 'Outcome_B']] 
    
    # Change HC to a class label 2
    mrs_i.loc[0:79,'Outcome_B'] = 2
    
    mrs_i_temp = mrs_i.iloc[80:170,:]  # reorder from P01-P13 then HC01-HC10 as in brains_outcomes.csv 
    mrs_i_temp = mrs_i_temp.append(mrs_i.iloc[0:80,:]) 
    
    mrs_i = mrs_i_temp
    mrs_i = mrs_i.reset_index(drop=True)
    del mrs_i_temp
    
    return mrs_i
    

    