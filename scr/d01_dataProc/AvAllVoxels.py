import pandas as np
import os

# My custom functions 
from d00_utils.saveFile import saveFileToDirPath 

def f_AvAllVoxels(AllVox):
    """
    USAGE: 
    f_AvAllVoxels() 
    
    DESCRIPTION:
    Averages all the voxel data for each patient     
    
    INPUTS:  
    AllVox - contains un vaverage voxel information 
    
    OUTPUTS: 
    AllVox_av - data set where voxels are average on a per person basis (no anatomical information) 
    """

    # remove Voxel number label
    AllVox = AllVox.drop(columns=['Voxel']) 
        
    # make new data frame to store the averaged voxel values for each patient  
    AllVox_av = AllVox.groupby(['Code'], sort=False).mean()     
    AllVox_av = AllVox_av.reset_index()
    
    
    AllVox_av = AllVox_av.round(2)
    
    
    # Export to intermidiate folder 
    outname = 'AllVox_av.csv'
    outdir = '../data/d02_intermediate/AllVox_av'    
    saveFileToDirPath(AllVox_av, outname, outdir, 1)

    
    

    return AllVox_av
