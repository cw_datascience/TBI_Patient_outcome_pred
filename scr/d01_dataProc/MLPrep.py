import pandas as pd
import numpy as np
import os

# My custom functions 
from d00_utils.saveFile import saveFileToDirPath 



def mergeAll_Seaons():

    l_season = ['2016-17', '2017-18', '2018-19', '2019-20', '2020-21', '2021-22']
    l_encoding_latin = ['latin-1', 'latin-1', 'latin-1', 'utf-8', 'utf-8', 'utf-8']   # from https://github.com/vaastav/Fantasy-Premier-League/blob/60fa27d609501ba06b1777489d40dbd9b59c5f3e/global_merger.py

    # Read individual season merged online data from vaastav
    # create a dictionary to store the data frames from each seaon
    dic_seasons = {}
    for season, encoding in zip(l_season, l_encoding_latin):
        dic_seasons.update({season: pd.read_csv('https://raw.githubusercontent.com/vaastav/Fantasy-Premier-League/master/data/' + season + '/gws/merged_gw.csv', encoding = encoding)}) #    'ISO-8859-1'

        # add a season columns
        dic_seasons[season]['season_x'] = season

        # check how many columns are in each seaons
        print('Season ', season, 'has ', len(dic_seasons[season].columns), ' features')

    del season, encoding     

    print('***')
    print('Decide which features to keep. Feautres that do not apear in all data frames must be dropped straigh away from and features that do not apeal in the smallest fearture set (2019-20). Droping .....')
    print('***')

# 
    for season in l_season: 
        
        FeatMask = np.isin(dic_seasons[season].columns, dic_seasons['2019-20'].columns)
        DropFeat = dic_seasons[season].columns[~FeatMask]
        
        # drop unwanted features 
        dic_seasons[season].drop(columns=DropFeat, inplace=True)
        # make feature order the same as the 2019-20 season
        
        print('***')

        print('Season ', season, ' now has ', len(dic_seasons[season].columns), ' features')
        
        print('Does season ', season, ' have same feature order as 2019-20 ?', all(dic_seasons[season].columns == dic_seasons['2019-20'].columns))
        

        del FeatMask
    
    # return a list containing the dataframe for each seaon
    return list(dic_seasons.values())







#   I am proably not going to us the the functions below now ... 
# I made these when using the megerged data set 
def f_MLPrep(ROI_data):
    """
    USAGE: f_MLPrep(mrs_i_ROI)
    
    
    DESCRIPTION:
    Take out put anatmoical data from f_anatomicalVox and organsies the data
    to be passed to algorithm   
    
    INPUTS:  
    ROI_data - out put form f_anatomicalVox
    
    OUTPUTS: 
    out - data read for ML 

    """

    ROI_data = ROI_data.drop(columns={'Voxel'})
    
    ROI_data = ROI_data.set_index(['Code','Outcome_B', 'Anatomical pos'], append=True)
    
    ROI_data = ROI_data.unstack(level=3)
    ROI_data = ROI_data.reset_index()
    ROI_data = ROI_data.drop(columns='level_0')
    
    #cw = mrs_i_ROI.loc[mrs_i_ROI.Code == 'HV01'].mean()
    
    # Average the specific ROI regions 
    ROI_data = ROI_data.groupby(['Code'], sort=False).mean()
    #dataAna_av_df.loc['HV01'] = a.loc[a.Code == 'HV01'].mean()
    
            
    
    
    # 'P05' does not have a value for Parital so fill with the average of central and frontal
    
    ROI_data.loc['P05',( 'sPCr_sATPg', 'Pariental')] = (ROI_data.loc['P05',( 'sPCr_sATPg', 'Central')] + ROI_data.loc['P05',( 'sPCr_sATPg', 'Frontal')])/2 
    
    ROI_data.loc['P05',( 'sATPg_All.1', 'Pariental')] = (ROI_data.loc['P05',( 'sATPg_All.1', 'Central')] + ROI_data.loc['P05',( 'sATPg_All.1', 'Frontal')])/2
    
    ROI_data.loc['P05',( 'sPCr_All.1', 'Pariental')] = (ROI_data.loc['P05',( 'sPCr_All.1', 'Central')] + ROI_data.loc['P05',( 'sPCr_All.1', 'Frontal')])/2 
    
    ROI_data.loc['P05',( 'spH', 'Pariental')] = (ROI_data.loc['P05',( 'spH', 'Central')] + ROI_data.loc['P05',( 'spH', 'Frontal')])/2 
    
    
    ROI_data = ROI_data.reset_index()

    # Export data to intermidiate folder 
    outname = 'ROI_data_prep.csv'
    outdir = '../data/d02_intermediate/ROI_data_prep'
    saveFileToDirPath(ROI_data, outname, outdir, 1)
        
    return ROI_data