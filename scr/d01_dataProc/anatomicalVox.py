import numpy as np
import pandas as pd
import os

# My custom functions 
from d00_utils.saveFile import saveFileToDirPath 


def f_anatomicalVox(AllVox):
    """
    mrs_i_ROI = f_anatomicalVox(mrs_i)

    USAGE: 
    out = f_anatomicalVox(AllVox)
    
    DESCRIPTION:
    Create a data set with anatomical info 
    where regions of average voxels correspond to ('Pariental', 'Central', 'Frontal') 
    
    INPUTS:  
    AllVox - data with anatmomical info before being registered to a 
    specifc region  
    
    OUTPUTS: 
    mrs_i_ROI - data where voxles havebeen grouping and averaged into '
    Pariental', 'Central', 'Frontal'have been  
    
    """


    mrs_i_ROI = AllVox # new data frame mrs_i_ROI - will contain anatomical positions 
    mrs_i_ROI['Anatomical pos'] = np.nan  # add a column to store the anatmical information 
    
    # apply the same anatomical voxel posttion are consistemt for all HC's
    # Frontal = 20,21,28,29  
    Frontal_l = [20,21,28,29,12,13,19,22,27,30]
    for Frontal in Frontal_l:
        mrs_i_ROI.loc[mrs_i_ROI.Voxel == Frontal, 'Anatomical pos'] = 'Frontal'
        
    # central = 28, 29   
    central_l = [36,37,35,38]
    for central in central_l:
        mrs_i_ROI.loc[mrs_i_ROI.Voxel == central, 'Anatomical pos'] = 'Central'
    
    # pariental = 44, 45
    pariental_l = [44,45,43,46]     
    for pariental in pariental_l:
        mrs_i_ROI.loc[mrs_i_ROI.Voxel == pariental, 'Anatomical pos'] = 'Pariental'
    
    del pariental, pariental_l, central, central_l, Frontal, Frontal_l
    
    # IMPORTANT: At this stage I have added the anitomical positions to each voxels however
    # I need to correct the assignemtns for patient: P07 where the grid has been 
    # shifted foward  (i.e grid starts at vox 12)
    
    # change voxel 28 and 29 to central 
    mrs_i_ROI.loc[40,'Anatomical pos'] = 'Central'
    mrs_i_ROI.loc[45,'Anatomical pos'] = 'Central'
    
    # change voxel 36 and 37 to Pariental 
    mrs_i_ROI.loc[38,'Anatomical pos'] = 'Pariental'
    mrs_i_ROI.loc[39,'Anatomical pos'] = 'Pariental'

    # Export to intermidiate folder 
    outname = 'mrs_i_ROI.csv'
    outdir = '../data/d02_intermediate/mrs_i_ROI'
    saveFileToDirPath(mrs_i_ROI, outname, outdir, 1)
    
    return mrs_i_ROI