# This scripts contains a python implimentatinos of the Metropolis-Hastings algorithm
# From: https://agustinus.kristia.de/techblog/2015/10/17/metropolis-hastings/


import numpy as np


def Metropolis_Hastings(p, iter=1000):
    x = 0.1
    samples = np.zeros((iter, 1))

    for i in range(iter):
        x_star = np.array([x]) + np.random.normal(size=1)
        if np.random.rand() < p(x_star) / p(x):
            x = x_star
        samples[i] = np.array([x])

    return samples