
import numpy as np
from scipy import interpolate 
import matplotlib.pyplot as plt


# import my custom libary
from d04_metrics.PostiriorBalACC import f_betaAvPDF


def splineConv(betaParams, res, plot=False, XLab='Balanced accuracy'):
    x = np.arange(0,1, res)

    convPDF = f_betaAvPDF(x, betaParams)
    convPDF = np.real(convPDF)
    
    t, c, k = interpolate.splrep(x, convPDF, s=0, k=4)
    spline = interpolate.BSpline(t, c, k, extrapolate=False)

    if plot == True: # Plot the spline fit 
        plt.plot(x, convPDF, 'bo', label='Original PDF points')
        plt.plot(x, spline(x), 'r', label='BSpline PDF')
        plt.grid()
        plt.title('Spline Fit to PDF')
        plt.xlabel(XLab)
        plt.ylabel('Density')
        plt.legend(loc='best')
        plt.show()
    return spline
