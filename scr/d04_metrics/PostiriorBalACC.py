from distutils.log import error
import numpy as np
from pandas import array 
import matplotlib.pyplot as plt
from scipy.stats import beta, norm

def f_betaAvPDF(x, params):
    
    numPDF = np.shape(params)[0]
    y = sumBetaPDF(numPDF * x, params)
    #print('Integral of convolution after before * numPDF = ', np.real(np.trapz(y, x)))
    #print('Integral of convolution numPDF * x = ', np.real(np.trapz(y, numPDF * x)))
    y = y * numPDF
    #print('Integral of convolution after end = ', np.real(np.trapz(y, x)))
    return y

   
    


def sumBetaPDF(x, params):
    # Convolve the beta distributions using the characteristic function
    res = 0.001; # precision
    c = convBeta(res, params)
    
    # Vector of desired outut size
    
    y = np.ones(len(x))
    y[:] = np.nan
    
    # Fill in return value
    numPDF = np.shape(params)[0]
    
    # values 
    # all other values
    idx = np.shape(x[0::numPDF])
    
    y1 = c[0::numPDF]
    #print('Integral of convolution after down sampling = ', np.real(np.trapz(y1, x)))
    #y = c[idx]
    print(np.shape(y1))

    return y1

    
    




def convBeta(res, params):

     # Set support
    numPDF = np.shape(params)[0]
    x = np.arange(0, numPDF, res)
    
    # Individual Beta pdfs
    deta_dst = np.ones([numPDF, len(x)])

    for i in list(range(0,numPDF)):
        deta_dst[i,:] = np.array([beta.pdf(x,params[i, 0], params[i, 1])])
    

    CF_sum = 1 /numPDF    

    # The sum of a set random variables is equivalent to the convolution of
    # their PDFs distributions
    for i in list(range(0,numPDF)):
        CF = np.fft.fft(deta_dst[i,:]) # CF = cahracteristc funciton
        CF_sum = CF_sum * CF
    del i

    # calc pdf 
    f_PDF = np.fft.ifft(CF_sum)

    # Normalize (so that values sum to 1/res)
    f_PDF = f_PDF / (sum(f_PDF) * res)

    #print('Aveage of the beta distributions = ', np.real(np.trapz(f_PDF, x)))

    #plt.plot(x, np.real(f_PDF))
    #plt.show()
    return f_PDF




def calcBetaParams(disp, Betaprior):
    # get number of sucesses - k (0 = badoutcome, 1=goodoutcome, 2=heathycontrol)
    k = np.diag(disp.confusion_matrix)

    # get total instances in each class - n
    n = np.sum(disp.confusion_matrix, axis=1)

    # get numnber of classes
    totClas = np.shape(disp.confusion_matrix)[0]

    # prealocate memoory to stor beta parameters
    betaParams = np.ones((totClas, 2))

    for i in range(0, totClas):
        betaParams[i,0] = k[i] + Betaprior.get('a')
        betaParams[i,1] = n[i] - k[i] + Betaprior.get('b')

    return betaParams




def f_plot_post(x, PDFDic, alpha, color, chance, fontsize=15, padSubplt=3.0):
    """
    f_plot_post 

    USAGE: 
    ax = f_plot_post(...)
    
    DESCRIPTION:
    Plots the postirior distribution with and alpha significance level
    and predfined chance level 
    
    INPUTS:  
    x - x axis inputs 
    PDF - A dict containg at leaste one PDF
    alpha - Defined the HDI
    chance - random chance level
    titleStr - create title string
    padSubplt - ammount of padding to put between subplots 
    

    
    OUTPUTS: 
    ax - axis object of the figure
    ... - ...
    """
    if type(PDFDic) != dict:
        print('Error: Put PDF in a dictionary!!!')
        return -1 

    # define the number of subplots I will need
    NumPDF = int(len(PDFDic))
    fig, ax = plt.subplots(1, NumPDF)
    fig.tight_layout(pad=padSubplt)

    for i, PDFKey in enumerate(PDFDic.keys()):
        PDF = PDFDic[PDFKey]

        # in the PDF is complex only us the real rumbers
        if not any(np.iscomplex(PDF)):
            PDF = np.real(PDF)

        # Compute alpha confidence inteval
        # consturt the CDF
        CDF = np.cumsum(PDF) / np.sum(PDF)

        # make the probabiliy ditribution 0 at the alpha/2 tails of the distribution
        CDF_CF = np.copy(CDF)
        CDF_CF[CDF < alpha/2] = 0
        CDF_CF[CDF > 1 - alpha/2] = 0

        # _, ax = plt.subplots(1,2)
        # ax[0].plot(x,CDF)
        # ax[0].fill(x, CDF_CF, 'b', alpha=0.3) # plot the 95% HDI
        # ax[0].title.set_text('central 95% of the CDF')
        # ax[0].set_xlabel('Balanced accuracy')  
        # ax[0].set_ylabel('Cumulative Probability')


        PDF_CF = np.copy(PDF)
        PDF_CF[CDF < alpha/2] = 0
        PDF_CF[CDF > 1 - alpha/2] = 0
        
        # plot postiror distribution with 95 % HDI
        ax[i].plot(x,PDF, ) # plots whole postirior distribution
        ax[i].fill(x, PDF_CF, 'b', alpha=0.3) # plot the 95% HDI
        titleStr = PDFKey + ' feature set'
        ax[i].title.set_text(titleStr)
        ax[i].set_xlabel('Balanced accuracy ($\lambda$)')
        ax[i].set_ylabel('Density')
        ax[i].set_ylim(0,6.2)

        # plot chance level line
        ax[i].plot([chance, chance], [0, 5], 'k--', linewidth=2)
        del PDF, CDF, PDF_CF, CDF_CF

    return ax
    



