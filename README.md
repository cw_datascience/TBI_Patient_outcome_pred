# Predicting clinical outcome of brain injury patients

### Introduction
##### **Background**
Traumatic brain injury (TBI) induces a complex play of pathological processes, manifesting as a broad spectrum of symptoms that can impact an individual's memory, thinking and personality. In vivo Magnetic Resonance Spectroscopy (MRS) allows for non-invasive probing of these cerebral metabolic derangements, offering a more refined understanding of the chemistry following injury.

##### **Motivation**
There is a need for establishing an unbiased means for predicting patient symptomatic outcomes at later phases following injury ($\geq$ 6 months). The heterogeneity of TBI is such that treatment for one group may not be suitable for another. Classification of a patient's expected outcome at the acute phase of injury could assist clinicians in selecting a more targeted treatment course for each patient, maximising the probability of improved clinical outcome.

##### **Results**
In conclusion this study indicates that $^{31}$P MRSI data acquired from acute phase TBI patients can be used to train machine learning algorithms to predict patient outcome six months post injury, while also distinguishing between healthy controls. Classifier performance was assessed by calculating the posterior distribution of the balanced accuracy from the nested leave-one-out cross-validation confusion matrix.

The best balanced accuracy was 81.5 [-15.5, +11.5] %. The SVM-RBF (support vector machine with radial basis function) algorithm was used here. Read ML_PatientOutcomePrediction_ThesisChapter.pdf for a full description of the work done for this project.  




##### **How to read the project**
The reader should follow the notebooks in the notebooks file. 
- **NB_01_dataProc.opynb** - where the data processing takes place
- **NB_02_Modeling.opynb** - where the models are build and analysed 



The source code written for this project is stored in the scr file.  



## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/cw_datascience/TBI_Patient_outcome_pred.git
git branch -M main
git push -uf origin main
```


## Name
Choose a self-explaining name for your project.


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.


